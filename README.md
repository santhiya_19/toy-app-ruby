
open a browser in your [localhost](http://localhost:3000/) and you should be able create new users, edit and destroy them 


![screenshot](app/assets/images/toy_app_users.png)

If you want to create new microposts, edit or destoy them you have to go to the [microposts](http://localhost:3000/microposts) page

![screenshot](app/assets/images/toy_app_microposts.png)
 

